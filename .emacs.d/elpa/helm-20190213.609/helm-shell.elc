;ELC   
;;; Compiled
;;; in Emacs version 26.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\306\307\310\311\312\313%\210\306\314\315\316\312\313%\210\317\320\321\322\323DD\324\312\325\326\327&\207" [require cl-lib helm helm-lib helm-help helm-elisp custom-declare-face helm-shell-prompts-promptidx ((t (:foreground "cyan"))) "Face used to highlight Shell prompt index." :group helm-shell-faces helm-shell-prompts-buffer-name ((t (:foreground "green"))) "Face used to highlight Shell buffer name." custom-declare-variable helm-shell-prompts-promptidx-p funcall function #[0 "\300\207" [t] 1] "Show prompt number." helm-shell :type boolean] 8)
#@37 Keymap for `helm-shell-prompt-all'.
(defvar helm-shell-prompts-keymap (byte-code "\301 \302\"\210\303\304\305#\210\303\306\307#\210\211\207" [helm-map make-sparse-keymap set-keymap-parent define-key "o" helm-shell-prompts-other-window "" helm-shell-prompts-other-frame] 5) (#$ . 1037))
#@190 List the prompts in Shell BUFFER.

Return a list of ("prompt" (point) (buffer-name) prompt-index))
e.g. ("ls" 162 "*shell*" 3).
If BUFFER is nil, use current buffer.

(fn &optional BUFFER)
(defalias 'helm-shell-prompts-list #[256 "r\211\206 pq\210\301=\205Y \212eb\210\302\303\304 \305\306\307\310\311!\312\"\313$\216\212\314\211\203Q m?\2050 \315\303!\211\203J \316\317 \"\320 FB\262T\262\202M \302\262\210\202$ \210*\210\237\266\202))\207" [major-mode shell-mode nil 1 save-mark-and-excursion--save make-byte-code 0 "\301\300!\207" vconcat vector [save-mark-and-excursion--restore] 2 t comint-next-prompt buffer-substring-no-properties point-at-eol buffer-name] 10 (#$ . 1337)])
#@71 List the prompts of all Shell buffers.
See `helm-shell-prompts-list'.
(defalias 'helm-shell-prompts-list-all #[0 "\300 \301\211:\203 @\262\302\303!!\244\262A\262\202 \211\237\207" [buffer-list nil reverse helm-shell-prompts-list] 6 (#$ . 2037)])
#@33 

(fn CANDIDATES &optional ALL)
(defalias 'helm-shell-prompts-transformer #[513 "\301\211\211\211\211\211:\203` @\262\211A\262\242\262\211A\262\242\262\211A\262\242\262@\262\205: \302\303\304#\305P\205G \302\306!\303\307#\305PQFBB\262A\262\202 \211\237\207" [helm-shell-prompts-promptidx-p nil propertize face helm-shell-prompts-buffer-name ":" number-to-string helm-shell-prompts-promptidx] 14 (#$ . 2299)])
#@19 

(fn CANDIDATES)
(defalias 'helm-shell-prompts-all-transformer #[257 "\300\301\"\207" [helm-shell-prompts-transformer t] 4 (#$ . 2752)])
#@57 

(fn CANDIDATE &optional (ACTION \='switch-to-buffer))
(defalias 'helm-shell-prompts-goto #[385 "\211\203 \211A\262\242\202 \300\203 \301\302\303\304G\\D\"\210\3048\305 \230\203, \300=\2040 !\210A@b\210\306 \262\207" [switch-to-buffer signal wrong-number-of-arguments helm-shell-prompts-goto 2 buffer-name recenter] 8 (#$ . 2897)])
#@18 

(fn CANDIDATE)
(defalias 'helm-shell-prompts-goto-other-window #[257 "\300\301\"\207" [helm-shell-prompts-goto switch-to-buffer-other-window] 4 (#$ . 3250)])
#@18 

(fn CANDIDATE)
(defalias 'helm-shell-prompts-goto-other-frame #[257 "\300\301\"\207" [helm-shell-prompts-goto switch-to-buffer-other-frame] 4 (#$ . 3417)])
(defalias 'helm-shell-prompts-other-window #[0 "\203 \301\302!\207\303\304!\207" [helm-alive-p helm-exit-and-execute-action helm-shell-prompts-goto-other-window error "Running helm command outside of context"] 2 nil nil])
(put 'helm-shell-prompts-other-window 'helm-only t)
(defalias 'helm-shell-prompts-other-frame #[0 "\203 \301\302!\207\303\304!\207" [helm-alive-p helm-exit-and-execute-action helm-shell-prompts-goto-other-frame error "Running helm command outside of context"] 2 nil nil])
(put 'helm-shell-prompts-other-frame 'helm-only t)
#@67 Pre-configured `helm' to browse the prompts of the current Shell.
(defalias 'helm-shell-prompts #[0 "\301=\203 \302\303\304\305\306\307\310 \311\312\313\314&\315\316$\207\317\320!\207" [major-mode shell-mode helm :sources helm-make-source "Shell prompts" helm-source-sync :candidates helm-shell-prompts-list :candidate-transformer helm-shell-prompts-transformer :action (("Go to prompt" . helm-shell-prompts-goto)) :buffer "*helm Shell prompts*" message "Current buffer is not an Shell buffer"] 11 (#$ . 4131) nil])
#@68 Pre-configured `helm' to browse the prompts of all Shell sessions.
(defalias 'helm-shell-prompts-all #[0 "\301\302\303\304\305\306\307 \310\311\312\313\314&\n\315\316$\207" [helm-shell-prompts-keymap helm :sources helm-make-source "All Shell prompts" helm-source-sync :candidates helm-shell-prompts-list-all :candidate-transformer helm-shell-prompts-all-transformer :action (("Go to prompt" . helm-shell-prompts-goto) ("Go to prompt in other window `C-c o`" . helm-shell-prompts-goto-other-window) ("Go to prompt in other frame `C-c C-o`" . helm-shell-prompts-goto-other-frame)) :keymap :buffer "*helm Shell all prompts*"] 13 (#$ . 4656) nil])
(provide 'helm-shell)
